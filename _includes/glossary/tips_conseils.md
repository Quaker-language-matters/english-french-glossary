{% if page.lang == "fr" %}
1. Les verbes réfléchis figurent dans le glossaire français&ndash;anglais à l'endroit correspondant au verbe lui-même, mais néanmoins précédés du pronom « se » ou « s'... ».
2. Les renvois à d'autres mots au sein du glossaire sont indiqués par des majuscules (ex.: « cf. CLERK »)
3. La note "**text.**" indique que l'expression en question est citée dans son contexte dans la section "[Quelques textes quakers souvent cités](/fr/cite)".
4. Les noms présentés avec un trait d'union, du genre « Ami-e », indiquent qu'« Ami » en est la forme masculine et qu'« Amie » en est la féminine.
5. Si aucun genre n'est indiqué, le nom peut être soit masculin, soit féminin.
6. En anglais, le glossaire suit la pratique des Nations Unies et respecte l'orthographe anglaise.
7. L'anglais recourt plus volontiers aux majuscules que le français. Les majuscules dans ce glossaire sont volontairement choisis pour correspondre à l'usage courant dans chaque langue.
{% else %}
1. In the French-English glossary, reflexive verbs are placed in the alphabetical position corresponding to the verb itself, but they are nonetheless preceded by "se" or "s'..."
2. Cross-references to other entries within the glossary are indicated in capitals (e.g.: 'cf. CLERK')
3. The reference '**text.**' indicates that the expression in question is presented in its context in the section '[Some often-quoted Quaker texts](/en/cite)'.
4. In French, nouns presented with a dash, e.g. "Ami-e" indicate that "Ami" is the masculine and "Amie" the feminine form.
5. If no gender is indicated, the noun can be either masculine or feminine.
6. In line with UN practice, the glossary uses British spelling in English.
7. English capitalises the initial letter of words more frequently than French. The capitals in this glossary have been chosen intentionally to correspond to normal usage in both French and English.
{% endif %}
