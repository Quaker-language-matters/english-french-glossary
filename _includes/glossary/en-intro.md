{% comment %}exclude_from_search: true{% endcomment %}
{% if page.trans == "glossary_fren" %}A French to English ({{ site.data.FR-glossaire-csv.size }} entries){% else %}An English to French ({{ site.data.EN-glossary-csv.size }} entries){% endif %} glossary of Quaker terms, created by Ed Dommen and digitized by [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"}.

Click any of the {% if page.trans == "glossary_fren" %}{{ site.data.glossaire.size }}{% else %}{{ site.data.glossary.size }}{% endif %} entries below to view definitions and commentary.
