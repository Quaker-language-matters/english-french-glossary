Un glossaire {% if page.trans == "glossary_fren" %}français-anglais (et [anglais-français](/fr/glossaire/ang-fr)) de termes Quaker ({{ site.data.FR-glossaire-csv.size }}{% else %}anglais-français (et [français-anglais](/fr/glossaire/fr-ang)) de termes Quaker ({{ site.data.EN-glossary-csv.size }}{% endif %} entrées), créé par Ed Dommen et mis en page par [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"}.

Cliquer sur les termes ci-dessous pour voir les définitions et les commentaires. 
