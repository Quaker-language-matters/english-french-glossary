<hr style="height:10px;visibility:hidden;margin:0"/>

## {{ site.short_title-fr }} 

Hébergé par le Comité consultatif mondial des Amis

{% for each in site.data.navigation.footer-fr %}[{{ each.title }}]({{ each.link }}){% unless forloop.last == true %} &ensp;{% endunless %}{% endfor %}

<br>
[![License CC](/assets/images/cc_logo.png){: img_center}](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr){: rel="noopener" target="_blank"}
