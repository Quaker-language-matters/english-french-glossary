---
title: Remerciements
lang: fr
trans: gloss_thx
---
Nous remercions Ed Dommen du groupe quaker de Genève pour l'usage de son glossaire, et [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"} de l'avoir converti en HTML. Avec toute question, [contactez-nous](/fr/contact) ou appelez Édouard Dommen en suisse au +41 22 774 1884.

Ce glossaire est disponible auprès du:

Comité consultatif mondial des Amis  
173 Euston Road  
London  
NW1 2AX  
Angleterre  
Tél: +44 20 7663 1199  
Fax: +44 20 7663 1189  
courriel: world@fwcc.world  
