---
layout: default
trans: home
lang: fr
og-title: Accueil
---
## Introduction

Un glossaire français-anglais de termes Quaker ({{ site.data.EN-glossary-csv.size }} entrées en anglais, {{ site.data.FR-glossaire-csv.size }} entrées en français), créé par Ed Dommen et mis en page par [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"} et David Summerhays de l'Assemblée de Montréal. Cette ressource est maintenant gérée par le Comité consultatif mondial des Amis et son comité de linguistique.
