---
title: Introduction
lang: fr
trans: gloss-intro
---
Ce glossaire ne contient que le jargon quaker, c.-à-d. les mots qui revêtent un sens particulier dans l'usage quaker. D'autres dictionnaires et glossaires existent pour le reste du langage.

Il est destiné avant tout à ceux et celles qui interprètent ou traduisent pour un public quaker. Pour d'autres publics, le recours au jargon quaker peut être inopportun. S'il est indispensable pour faire passer le message, dans les traductions écrites une note explicative peut rendre service au lecteur.

Ce glossaire s'est servi des ouvrages suivants:

* Ansermoz, Violette, _Le culte quaker_, Paris, Société religieuse des Amis, 1952
* Ceresole, Pierre, _Vivre sa vérité_, Neuchâtel, La Baconnière, 1950
* Dommen, Edouard, _Les Quakers_, Paris, Le Cerf, 1990
* Etten, Henry van, _George Fox et les quakers_, Paris, Le Seuil, 1956
* FWCC Section of the Americas, _Glosario cuáquero-Quaker Glossary_, Philadelphia, 1995
* FWCC, _Interpreting Quaker Experience-Témoignages quakers_, London, 1946
* _Le Petit Robert_, Paris, 1994
* Liens, Georges (traducteur), _Robert Barclay: la lumière intérieure, source de vie_, Paris, éditions Dervy, 1993
* _Précis des règles de discipline chrétienne, adoptées par la Société des Amis, connus sous le nom de quakers, de Congeniés, et autres lieux environnans, Dans les années 1785, 1801 et 1807_, avec une introduction et notes de Georges Liens, Luxembourg 1988
* _Quaker Faith & Practice_, Britain YM 1995
* Royston, Michael, "Réflexions sur deux notes du livre de Henry van Etten sur le quakerisme français" in _Lettre des Amis_ nº 77, juin 2003

Les quakers francophones ont moins recours à une terminologie propre que les anglophones; ils se servent plus volontiers du langage ambiant. Par conséquent, le glossaire français–anglais est plus court que l'anglais–français.

Que le rédacteur ait reçu bien moins d'observations sur le glossaire français–anglais que sur l'anglais–français reflète sans doute cette même réalité.

Toutes celles et tous ceux qui ont déjà offert des observations sur le glossaire méritent la gratitude non seulement du rédacteur mais surtout de tous les usagers de l'outil. Cela dit, le rédacteur porte la responsabilité de toute erreur, maladresse ou omission.

Un glossaire n'est jamais définitif. Le rédacteur recevra toujours volontiers des observations et des recommandations qui permettront d'améliorer ce glossaire-ci et de le maintenir à jour. N'hésitez surtout pas à communiquer vos observations et recommendations par notre [page de contact](/fr/contact) ou à:

Edward Dommen  
100, chemin des Mollies  
CH-1293 Bellevue, Switzerland  
Tel: +41 22 774 1884  
