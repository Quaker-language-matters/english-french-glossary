---
title: <i class="fas fa-universal-access fa-fw color-1-dark-text"></i> Informations sur l'accessibilité
lang: fr
trans: accessibility
og-title: Informations sur l'accessibilité
---
Le Quakerisme se base sur le respect de chaque être humain. Si nous pouvons améliorer votre accès à notre site web, nous espérons que [vous nous le ferez savoir](/fr/contact).

Veuillez [nous contacter](/fr/contact) si vous avez besoin d'informations supplémentaires.

## Notre site web
* Si vous avez des difficultés à lire une ou des portion(s) de notre site web (par exemple votre lecteur d'écran fonctionne mal), [contactez-nous](/fr/contact) avec les détails et il nous fera plaisir de vous envoyer les sections concernées de notre site web en format Word, PDF ou autre format à votre convenance.
* Si votre navigateur web ne parvient pas à modifier correctement les couleurs ou la taille de la police sur notre site web, [contactez-nous](/fr/contact).
* Ce site web vise à respecter ou à dépasser les [normes d'accessibilité WAI du W3C](https://www.w3.org/WAI/standards-guidelines/fr){: rel="noopener" target="_blank"}.

## Remerciements
Nous remercions l'équipe de [l'Agence SAT](https://agencesat.com/){: rel="noopener" target="_blank"} pour l'audit en accessibilité, qui a grandement amélioré ce site web.
