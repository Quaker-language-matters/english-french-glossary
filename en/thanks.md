---
title: Acknowledgements
lang: en
trans: gloss_thx
---
Thanks to Ed Dommen of Geneva Meeting for faithfully maintaining this glossary, and to [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"} for converting it to HTML, and to the many Friends who have assisted with their comments and suggestions. [Contact us](/en/contact) or get in touch with Ed in Switzerland at +41 22 774 1884 with any questions or additions. 

This glossary is available from:

Friends World Committee for Consultation  
173 Euston Road  
London  
NW1 2AX  
England  
Tel: +44 20 7663 1199  
Fax: +44 20 7663 1189  
e-mail: world@fwcc.world  
UK Reg. Charity no. 211647  
