---
layout: default
lang: en
trans: home
permalink: /en/home
og-title: Home
---
## Introduction 
An English-French glossary of Quaker terms ({{ site.data.EN-glossary-csv.size }} entries in English, {{ site.data.FR-glossaire-csv.size }} entries in French), created by Ed Dommen of Geneva Meeting and digitized by [Simon Grant](http://www.simongrant.org/home.html){: rel="noopener" target="_blank"} and David Summerhays of Montreal Meeting. This invaluable resource is now under the care of Friends World Committee for Consultation (FWCC) and the Language Matters working group.
