---
title: Introduction
lang: en
trans: gloss-intro
---
This glossary contains only Quaker jargon, i.e. words used in a special sense among Quakers. Other words are ignored here because they can be found in other dictionaries and glossaries.

It is intended primarily for people interpreting or translating for a Quaker public. For a non-Quaker public the use of Quaker terminology may not be appropriate. If it is is essential to the message, in written translation an explanatory footnote may help the reader.

This glossary has drawn on the following works:

* Ansermoz, Violette, _Le culte quaker_, Paris, Société religieuse des Amis, 1952
* Ceresole, Pierre, _Vivre sa vérité_, Neuchâtel, La Baconnière, 1950
* Dommen, Edouard, _Les Quakers_, Paris, Le Cerf, 1990
* Etten, Henry van, _George Fox et les quakers_, Paris, Le Seuil, 1956
* FWCC Section of the Americas, _Glosario cuáquero-Quaker Glossary_, Philadelphia, 1995
* FWCC, _Interpreting Quaker Experience-Témoignages quakers_, London, 1946
* _Le Petit Robert_, Paris, 1994
* Liens, Georges (traducteur), _Robert Barclay: la lumière intérieure, source de vie_, Paris, éditions Dervy, 1993
* _Précis des règles de discipline chrétienne, adoptées par la Société des Amis, connus sous le nom de quakers, de Congeniés, et autres lieux environnans, Dans les années 1785, 1801 et 1807_, avec une introduction et notes de Georges Liens, Luxembourg 1988
* _Quaker Faith & Practice_, Britain YM 1995
* Royston, Michael, "Réflexions sur deux notes du livre de Henry van Etten sur le quakerisme français" in _Lettre des Amis_ nº 77, juin 2003

French-speaking Friends have less jargon than English-speaking ones; they make readier use of current language. As a result, the French–English glossary is shorter than the English–French one.

The fact that distinctly fewer comments have been received on the French–English than on the English–French glossary undoubtedly reflects the same reality.

All those who have already offered comments on the glossary deserve the thanks not only of its compiler, but especially of all its users. That having been said, the compiler is responsible for any errors, infelicities or omissions.

A glossary is never finished. Comments and suggestions about this glossary are always welcome to improve this one and keep it up to date. They can be sent directly to [our contact page](/en/contact) and also to:

Edward Dommen  
100, chemin des Mollies  
CH-1293 Bellevue, Switzerland  
Tel: +41 22 774 1884  
